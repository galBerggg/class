import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './interfaces/User';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
user: Observable<User | null>

  constructor(public afAuth:AngularFireAuth) { 
    this.user = this.afAuth.authState;
  }

signup(email:string, password:string){
  this.afAuth
      .auth
      .createUserWithEmailAndPassword(email,password)
      .then(res => console.log('Succesful Signup', res))
}

login(email:string, password:string){
  this.afAuth
      .auth
      .signInWithEmailAndPassword(email,password)
      .then(res => console.log('Succesful Signup', res))
}





}
